"use strict";

/*    1.   Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
        
            коментарі, спецсимволи   

      2.    Які засоби оголошення функцій ви знаєте?      
            function(){}
            const func = function(){}
            const func = () => {}

            const obj = {
              name: "vetal";
              greet: function() {
                console.log(`Hello, ${this.name}!`);

      3.    Що таке hoisting, як він працює для змінних та функцій?      
            підняття, змінних в області видтмості з функцій заданих через function
                 */

let newUser = {};

let createNewUser = () => {
  newUser = {
    _firstName: prompt("Введіть ім'я:"),
    _lastName: prompt("Введіть прізвище:"),
    birthday: prompt("Введіть дату народження:"),

    get firstName() {
      return this._firstName;
    },

    get firstName() {
      return this._lastName;
    },

    setFirstName: function (newFirstName) {
      this._firstName = newFirstName;
    },
    setLastName: function (newLastName) {
      this._lastName = newLastName;
    },
    getLogin: function () {
      return `${this._firstName[0]}${this._lastName}`.toLowerCase();
    },
    getAge: function () {
      const [day, month, year] = this.birthday.split(".");
      const dob = new Date(year, month - 1, day);
      const today = new Date();
      const diffMs = today - dob;
      const age = Math.floor(diffMs / 31557600000); // 31557600000 - мілісекунд у році

      return age;
    },
    getPassword: function () {
      const [day, month, year] = this.birthday.split(".");
      const firstNameInitial = this._firstName[0].toUpperCase();
      const lastName = this._lastName.toLowerCase();
      const birthYear = year;

      return firstNameInitial + lastName + birthYear;
    },
  };
  return newUser;
};

Object.defineProperty(newUser, "_firstName", {
  writable: false,
});
Object.defineProperty(newUser, "_lastName", {
  writable: false,
});

console.log(createNewUser());
console.log(newUser.getAge());
console.log(newUser.getPassword());
